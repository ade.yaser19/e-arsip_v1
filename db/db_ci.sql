-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 28 Jul 2023 pada 23.27
-- Versi server: 10.4.28-MariaDB
-- Versi PHP: 8.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ci`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`id`, `title`, `icon`, `url`, `is_active`) VALUES
(1, 'Dashboard', 'fa fa-tachometer', 'dashboard', 1),
(2, 'Pengguna', 'fa fa-users', 'users', 1),
(4, 'Menu', 'fa fa-navicon', 'menu', 1),
(5, 'Pengaturan', 'fa fa-gear', 'pengaturan', 1),
(6, 'Role', 'fa fa-shield', 'role', 1),
(7, 'Role Menu', 'fa fa-address-card', 'access', 1),
(15, 'IDE E-Arsip', 'fa fa-file', 'arsip', 1),
(16, 'Approval E-Arsip', 'fa fa-file', 'approve_arsip', 0),
(17, 'Approval Dokumen', 'fa fa-file', 'approve_dok', 0),
(18, 'Upload E-Arsip', 'fa fa-file', 'upload', 0),
(19, 'Peminjaman Dokumen', 'fa fa-file', 'Dpinjaman', 0),
(20, 'Master E-Arsip', 'fa fa-file', 'M_earsip', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id` int(11) NOT NULL,
  `nama_sistem` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `pengaturan`
--

INSERT INTO `pengaturan` (`id`, `nama_sistem`) VALUES
(1, 'E-Arsip');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'Admin'),
(2, 'Petugas'),
(3, 'user'),
(5, 'Testing');

-- --------------------------------------------------------

--
-- Struktur dari tabel `th_id_arsip`
--

CREATE TABLE `th_id_arsip` (
  `id` varchar(8) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `no_ktp` varchar(16) NOT NULL,
  `no_hp` varchar(16) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `loan` varchar(30) NOT NULL,
  `produk` varchar(100) NOT NULL,
  `plafound` bigint(20) NOT NULL,
  `tanggal_cair` date NOT NULL,
  `Developer` varchar(100) NOT NULL,
  `nama_proyek` varchar(300) NOT NULL,
  `cabang` varchar(50) NOT NULL,
  `status` char(1) NOT NULL,
  `created_date` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `th_id_arsip`
--

INSERT INTO `th_id_arsip` (`id`, `nama`, `no_ktp`, `no_hp`, `Email`, `loan`, `produk`, `plafound`, `tanggal_cair`, `Developer`, `nama_proyek`, `cabang`, `status`, `created_date`) VALUES
('AP000001', 'NURDIAN HERLAMBANG MAULANA', '3216063105970014', '123', 'nurdianherlambang.as@gmail.com', '1', 'PRB', 1, '2023-07-27', 'MAS', 'Rumah Hunian', 'CIKARANG', 'R', '2023-07-27'),
('AP000002', 'Yanti Rahayu', '3222256784563001', '08313042061', 'rahayuyanti034@gmail', 'LD123456789', 'PRB', 350, '2020-02-22', 'MAS', 'Britania Bekasi', 'TIMUR', 'A', '2023-07-28'),
('AP000003', 'Hanna Astri Muldiyanti', '2111456745600001', '083256746578', 'hanna@gmail.com', 'LD987654321', 'PRB', 340000, '2022-02-22', 'MAS', 'Britania Bekasi', 'TIMUR', 'R', '2023-07-28'),
('AP000004', 'Hanna Astri Muldiyanti', '2387666758913001', '082154676577', 'hanna@gmail.com', 'LD987654321', 'PRB', 340000000, '2022-02-22', 'MAS', 'Britania Bekasi', 'TIMUR', 'A', '2023-07-28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_pd`
--

CREATE TABLE `tr_pd` (
  `id` int(11) NOT NULL,
  `surat_Permohonan` varchar(300) NOT NULL,
  `nama_nasabah` varchar(255) NOT NULL,
  `no_ktp` varchar(16) NOT NULL,
  `no_loan` varchar(50) NOT NULL,
  `tanggal_pencairan` date NOT NULL,
  `alasan` text NOT NULL,
  `status` char(1) NOT NULL,
  `created_date` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `tr_pd`
--

INSERT INTO `tr_pd` (`id`, `surat_Permohonan`, `nama_nasabah`, `no_ktp`, `no_loan`, `tanggal_pencairan`, `alasan`, `status`, `created_date`) VALUES
(1, 'Testing', 'Nurdian', '3216063105970014', 'LDKPS9090909', '2023-07-27', 'Testing permohonan', 'A', '2023-07-27'),
(2, 'Peminjaman Dokumen Jaminan', 'Yanti Rahayu', '3254678967200001', 'LD123456789', '2020-02-22', 'Restrukturisasi', 'A', '2023-07-28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ttd_id_arsip`
--

CREATE TABLE `ttd_id_arsip` (
  `id` int(11) NOT NULL,
  `nama_dokumen` varchar(300) NOT NULL,
  `link_dokumen` varchar(100) NOT NULL,
  `id_arsip` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `ttd_id_arsip`
--

INSERT INTO `ttd_id_arsip` (`id`, `nama_dokumen`, `link_dokumen`, `id_arsip`) VALUES
(1, 'SP3', 'HRIS.pdf', 'AP000002');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  `img` varchar(128) NOT NULL,
  `date_created` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `role_id`, `nama`, `email`, `password`, `img`, `date_created`) VALUES
(4, 1, 'admin', 'admin@admin.com', '$2y$10$wKPD3q3p4zhQrISUcHfRTeSLPQY9VyvQxilWMTxEG9MzzessAFK9O', 'images.png', '2023-07-24'),
(8, 2, 'petugas', 'petugas@petugas.com', '$2y$10$oAdBYej2nXsqqoI8aEXL/OWlsguZCf0Cuh7tV.OMTXUV/QCAMfdAa', 'default.png', '2023-07-24'),
(9, 3, 'adit', 'adit@gmail.com', '$2y$10$ssbRmLrX996Q1BR6SS8c7Ofh8fQfy2Kr1koUmgna28L4Zqa0pvMk6', 'default.png', '2023-07-24'),
(10, 3, 'Nurdian', 'nurdian@gmail.com', '$2y$10$wKPD3q3p4zhQrISUcHfRTeSLPQY9VyvQxilWMTxEG9MzzessAFK9O', 'default.png', '2023-07-24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(3, 1, 4),
(4, 3, 1),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(11, 1, 2),
(20, 2, 1),
(21, 1, 13),
(22, 2, 13),
(23, 1, 14),
(24, 2, 14),
(25, 3, 2),
(26, 3, 4),
(27, 1, 15),
(28, 1, 16),
(29, 1, 17),
(30, 1, 18),
(31, 1, 19),
(32, 1, 20);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `th_id_arsip`
--
ALTER TABLE `th_id_arsip`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tr_pd`
--
ALTER TABLE `tr_pd`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ttd_id_arsip`
--
ALTER TABLE `ttd_id_arsip`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `pengaturan`
--
ALTER TABLE `pengaturan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tr_pd`
--
ALTER TABLE `tr_pd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `ttd_id_arsip`
--
ALTER TABLE `ttd_id_arsip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
