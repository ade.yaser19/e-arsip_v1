<?= $this->session->flashdata('message'); ?>
<div class="row">
    <div class="conttainer">
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?php echo $this->db->count_all('th_id_arsip'); ?></h3>

                    <p>Approval E-Arsip</p>
                </div>
                <div class="icon">
                    <i class="fa fa-file fa-sm"></i>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box">
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style="text-align: center; width:5px;">No</th>
                    <th>No E-Arsip</th>
                    <th>Tanggal</th>
                    <th>Nama Nasabah</th>
                    <th>Loan</th>
                    <th>Cabang</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $x = 1;
                foreach ($approve_arsip as $v) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $x++; ?></td>
                        <td><?= $v['id']; ?></td>
                        <td><?= $v['created_date']; ?></td>
                        <td><?= $v['nama']; ?></td>
                        <td><?= $v['loan']; ?></td>
                        <td><?= $v['cabang']; ?></td>
                        <?php if ($v['status'] == 'P') { ?>
                        <td style="text-align: center;">
                            <a href="<?= base_url('approve_arsip/edit/') . $v['id']; ?>" class="btn btn-flat btn-xs btn-info">Approval</a>
                        </td>
                        <?php } else { ?>
                        <td style="text-align: center;">
                            <a href="<?= base_url('approve_arsip/detail/') . $v['id']; ?>" class="btn btn-flat btn-xs btn-success">Detail</a>
                        </td>
                        <?php } ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->