<?= validation_errors(
    '<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
    '</div>'
); ?>


<div class="box">
    <div class="box-body">
        <form action="<?= base_url('approve_arsip/edit'); ?>" class="form" method="POST">

            <div class="row" style="margin-top:-10px;"> 
                <div class="col-sm-6">
                    <h5>Data Nasabah :</h5>
                </div>
            </div>

            <br/>
            
            <div class="row">
                <div class="col-sm-6">
                    
                    <div class="form-group">
                        <label>No E-Arsip</label>
                        <input type="text" class="form-control" name="id" value="<?= $id ?>" readonly>
                    </div>

                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="nama" value="<?= $nama ?>" readonly>
                    </div>

                    <div class="form-group">
                        <label>No KTP</label>
                        <input type="text" class="form-control" name="no_ktp" value="<?= $no_ktp ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>No. HP</label>
                        <input type="text" class="form-control" name="no_hp" value="<?= $no_hp ?>" readonly>
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" value="<?= $Email ?>" readonly>
                    </div>
                </div>
            </div>

            <br/>

            <div class="row" style="margin-top:-10px;"> 
                <div class="col-sm-6">
                    <h5>Data Pembiayaan :</h5>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Loan</label>
                        <input type="text" class="form-control" name="loan" value="<?= $loan ?>" readonly>
                    </div>

                    <div class="form-group">
                        <label>Produk</label>
                         <input type="text" name="produk" class="form-control" value="<?= $produk ?>" readonly>
                    </div>

                    <div class="form-group">
                        <label>Plafond</label>
                        <input type="text" class="form-control" name="plafound" value="<?= $plafound  ?>" readonly>
                    </div>

                    <div class="form-group">
                        <label>Tanggal Cair</label>
                        <input type="text" class="form-control" name="tanggal_cair" value="<?= date('d-m-Y', strtotime($tanggal_cair)) ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Developer</label>
                        <input type="text" name="Developer" class="form-control" value="<?= $Developer?>" readonly>
                    </div>

                    <div class="form-group">
                        <label>Nama Proyek</label>
                        <input type="text" class="form-control" name="nama_proyek" value="<?= $nama_proyek ?>" readonly>
                    </div>

                    <div class="form-group">
                        <label>Cabang</label>
                        <input type="text" name="cabang" class="form-control" value="<?= $cabang?>" readonly>
                    </div>

                </div>
            </div>
            <a href="<?= base_url('approve_arsip') ?>" class="btn btn-sm btn-warning" style ="float: right; margin-left: 5px;">Kembali</a>
            <a href="<?= base_url('approve_arsip/reject/'.$id) ?>" class="btn btn-sm btn-danger" style ="float: right; margin-left: 5px;">Reject</a>
            <a href="<?= base_url('approve_arsip/approve/'.$id) ?>" class="btn btn-sm btn-primary" style ="float: right;">Approve</a>
        </form> 
    </div>
</div>