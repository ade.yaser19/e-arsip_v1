<?= validation_errors(
    '<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
    '</div>'
); ?>


<div class="box">
    <div class="box-body">
        <form action="<?= base_url('approve_dok/edit'); ?>" class="form" method="POST">
            <input type="hidden" name="id" value="<?= $id; ?>">
            
            <div class="row">
                <div class="col-sm-6"> 
                    <div class="form-group">
                        <label>Surat Permohonan</label>
                        <input type="text" class="form-control" name="surat_Permohonan" value="<?= $surat_Permohonan ?>" readonly>
                    </div>

                    <div class="form-group">
                        <label>Nama Nasabah</label>
                        <input type="text" class="form-control" name="nama_nasabah" value="<?= $nama_nasabah ?>" readonly>
                    </div>

                    <div class="form-group">
                        <label>No KTP</label>
                        <input type="text" class="form-control" name="no_ktp" value="<?= $no_ktp ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>No. Loan</label>
                        <input type="text" class="form-control" name="no_loan" value="<?= $no_loan ?>" readonly>
                    </div>

                    <div class="form-group">
                        <label>Tanggal Pencairan</label>
                        <input type="text" class="form-control" name="tanggal_pencairan" value="<?= date('d-m-Y', strtotime($tanggal_pencairan)) ?>" readonly>
                    </div>
                    
                    <div class="form-group">
                        <label>Alasan</label>
                        <input type="text" class="form-control" name="alasan" value="<?= $alasan?>" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Status</label>
                        <?php if ($status == 'Reject') { ?>
                        <input type="text" class="form-control" name="status" value="<?= $status ?>" readonly style="background-color:red; color:white;">
                        <?php } else { ?>
                            <input type="text" class="form-control" name="status" value="<?= $status ?>" readonly style="background-color:green; color:white;">
                        <?php } ?>
                    </div>
                </div>
            </div>

            <a href="<?= base_url('approve_dok') ?>" class="btn btn-sm btn-warning" style ="float: right; margin-left: 5px;">Kembali</a>
        </form> 
    </div>
</div>