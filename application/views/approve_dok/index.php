<?= $this->session->flashdata('message'); ?>
<div class="row">
    <div class="conttainer">
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?php echo $this->db->count_all('th_id_arsip'); ?></h3>

                    <p>Approval Dokumen</p>
                </div>
                <div class="icon">
                    <i class="fa fa-file fa-sm"></i>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="box">
    <div class="box-body">
        <form method="POST">
            <div class="box-body col-xs-12">
                  <div class="form-group row">
                    <label for="nama" class="col-sm-1 col-form-label">Nama</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="nama" name ='nama' placeholder="Nama">
                    </div>
                </div>    

                <div class="form-group row">
                    <label for="tanggal" class="col-sm-1 col-form-label">Tanggal</label>
                    <div class="col-sm-8">
                      <input type="date" class="form-control" id="tanggal" name="tanggal">
                    </div>
                </div>     

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10 ">
                      <input type="submit" name="submit" value=" Cari" class="btn btn-sm btn-flat btn-primary">

                      <a href="<?= base_url('approve_dok'); ?>" class="btn btn-sm btn-flat btn-warning">Reset</a> 
                    </div>
                </div>      
            </div>
        </form>
    </div>
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style="text-align: center; width:5px;">No</th>
                    <th>Tanggal</th>
                    <th>Nama Nasabah</th>
                    <th>Keterangan</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $x = 1;
                foreach ($approve_dok as $v) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $x++; ?></td>
                        <td><?= $v['created_date']; ?></td>
                        <td><?= $v['nama_nasabah']; ?></td>
                        <td><?= $v['alasan']; ?></td>
                        <?php if ($v['status'] == 'P') { ?>
                        <td style="text-align: center;">
                            <a href="<?= base_url('approve_dok/edit/') . $v['id']; ?>" class="btn btn-flat btn-xs btn-info">Approval</a>
                        </td>
                        <?php } else { ?>
                        <td style="text-align: center;">
                            <a href="<?= base_url('approve_dok/detail/') . $v['id']; ?>" class="btn btn-flat btn-xs btn-success">Detail</a>
                        </td>
                        <?php } ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->