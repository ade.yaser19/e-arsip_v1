<div class="login-box">
    <!-- <div class="login-logo">
        <a href="#" style="font-size: 18px; background-color: pink;">Login "<?= $nama['nama_sistem']; ?> Digital" </a>
    </div -->
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg" style="background-color: #179D97; color: white; padding-top: 20px; font-size: 18px;">Login "E-Arsip Digital"</p>
        <br/>
        <img src="<?= base_url('assets/img/logo/bsi.png') ?>" style="width: 100px; float:right; margin-top: -5px; margin-left: -5px; margin-bottom: -10px;">
        <br/>
        <br/>
        <form action="<?= base_url('auth'); ?>" method="post">
            <div class="form-group has-feedback">
                <input type="email" name="email" id="email" class="form-control" placeholder="Email" value="<?= set_value('email'); ?>">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <br/>
            
            <div class="row">
                <!-- /.col -->
                <div class="col-xs-4" style="float: right;">
                    <button type="submit" class="btn btn-primary btn-block btn-flat" style="background-color: #179D97;">Masuk</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
        <!-- <a href="<?= base_url('auth/register'); ?>" class="text-center">Register a new membership</a> -->

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->