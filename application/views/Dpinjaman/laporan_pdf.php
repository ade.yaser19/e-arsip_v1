<!DOCTYPE html>
<html>
	<head>
		<title>BERITA ACARA SERAH TERIMA DOKUMEN</title>
		<style type="text/css">
			@font-face {
	            font-family: 'myTahoma' !important;
	            src: url('<?= base_url('assets/fonts/tahoma.ttf') ?>') !important;
	        }
	        body {
	            font-family: 'myTahoma' !important;
	        }
			.header {
				width: 100%;
				text-align: center;
				margin: 20px auto;
				font-size: 14px;
				font-weight: bold;
			}
			.header p{
				font-size: 12px;
				margin-top: 5px;
				text-transform: none;
			}
			.center{
				width: 100%;
				text-align: center;
				margin: -10px auto;
				font-size: 18px;
				font-weight: bold;
			}
			.center p{
				font-size: 14px;
				margin-top: 5px;
				text-transform: none;
				text-decoration: none;
			}

			table {
				width: 100%;
				border-collapse: collapse;
			}
			table thead tr th,
			table tbody tr td {
				padding: 4px;
			}
			table thead tr th { text-align: center; }
			
			.sign {
				width: 100%;
				margin:20px 0;
				position: relative;
				min-height: 50px;
			}
			.sign .left {
				width: 25%;
				min-height: 50px;
				float: left;
				text-align: center;
			}
			.sign .middle {
				width: 50%;
				min-height: 50px;
				float: left;
			}
			.sign .right {
				width: 25%;
				min-height: 50px;
				float: left;
				text-align: center;
			}
			.sign p.title { font-size: 16px; font-weight: bold; }
			.sign p.name { 
				font-size: 18px; 
				text-transform: uppercase; 
				border-bottom: 1px solid black; 
				padding-bottom: 4px; 
				margin-bottom: 0; 
			}
			.sign p.grade {
				font-size: 16px;
				font-weight: bold;
				margin-top: 5px;
				margin-bottom: 0;
			}
		</style>
	<?php 
		function hariIndo ($hariInggris) {
		  switch ($hariInggris) {
		    case 'Sunday':
		      return 'Minggu';
		    case 'Monday':
		      return 'Senin';
		    case 'Tuesday':
		      return 'Selasa';
		    case 'Wednesday':
		      return 'Rabu';
		    case 'Thursday':
		      return 'Kamis';
		    case 'Friday':
		      return 'Jumat';
		    case 'Saturday':
		      return 'Sabtu';
		    default:
		      return 'hari tidak valid';
		  }
		}
	?>
	</head>
		<body>

		<div class="row">
			<div class="col-sm-12" style="margin-bottom: 20px;  margin-top: -40px;">
				<div class="center" style="text-transform: uppercase; font-size: 12px; font-weight: bold;">
					<h4>BERITA ACARA SERAH TERIMA DOKUMEN</h4>
				</div><!-- / HEADER -->	
			</div>
		</div>

		<table border="0" style="font-size: 12px; font-weight: normal; text-align: justify;">
			<tr>
		        <td style="height: 25px; padding-left: 5px;">Menunjuk permohonan <?= $data->cabang ?> perihal <?= $data->surat_Permohonan ?>. Pada hari <?= hariIndo(date('l', strtotime($data->created_date))) ?>, tangal <?= date('d-m-Y', strtotime($data->created_date)) ?>, dilakukan serah terima dokumen nasabah a/n <?= $data->nama_nasabah ?>, antara para pihak dibawah ini : </td>
		    </tr>
		</table>
		<br/>
		
		<table border="0" style="font-size: 12px; margin-left: -5px;">
			<tr>
		        <td colspan="1" style="width: 18%; border-right: none; height: 25px; padding-left: 10px;">Nama </td>
		        <td colspan="2" style="border-left: none; height: 25px;">: Rini Novita</td>
		    </tr>

		    <tr>
		        <td colspan="1" style="width: 18%; border-right: none; height: 25px; padding-left: 10px;">Jabatan </td>
		        <td colspan="2" style="border-left: none; height: 25px;">: Officer Pembiayaan</td>
		    </tr>

		    <tr>
		        <td colspan="1" style="width: 18%; border-right: none; height: 25px; padding-left: 10px;">Selanjutnya disebut </td>
		        <td colspan="2" style="border-left: none; height: 25px; font-weight: bold;"><b>PIHAK PERTAMA</b></td>
		    </tr>
		</table>
		<br/>
		
		<table border="0" style="font-size: 12px;  margin-left: -5px;">
			<tr>
		        <td colspan="1" style="width: 18%; border-right: none; height: 25px; padding-left: 10px;">Nama </td>
		        <td colspan="2" style="border-left: none; height: 25px;">: Dwita Dellani</td>
		    </tr>

		    <tr>
		        <td colspan="1" style="width: 18%; border-right: none; height: 25px; padding-left: 10px;">Jabatan </td>
		        <td colspan="2" style="border-left: none; height: 25px;">: CBS</td>
		    </tr>

		    <tr>
		        <td colspan="1" style="width: 18%; border-right: none; height: 25px; padding-left: 10px;">Selanjutnya disebut </td>
		        <td colspan="2" style="border-left: none; height: 25px;"><b>PIHAK KEDUA</b></td>
		    </tr>
		</table>
		<br/>
		
		<table border="0" style="font-size: 12px; text-align: justify;">
			<tr>
		        <td style="height: 25px; padding-left: 5px;"><b>PIHAK PERTAMA</b> dengan ini menyerahkan kepda <b>PIHAK KEDUA</b>, sebagaimana <b>PIHAK KEDUA</b> menerima penyerahan dari <b>PIHAK PERTAMA</b>, atas dokumen pada daftar terlampir. </td>
		    </tr>
		</table>

		<table border="0" style="font-size: 12px; text-align: justify; margin-top: 10px;">
			<tr>
		        <td style="height: 25px; padding-left: 5px;">Dengan penyerahan dokumen tersebut diatas, maka sejak tanggal ditandatanganinya Berita Acara Serah Terima ini, segala tanggung jawab atas penyimpanan, keberadaan dan keamanan dokumen dimaksud menjadi beralih kepada PIHAK KEDUA.</td>
		    </tr>
		</table>

		<table border="0" style="font-size: 12px; text-align: justify; margin-top: 10px;">
			<tr>
		        <td style="height: 25px; padding-left: 5px;">Demikian Berita Acara Serah Terima Dokumen ini dibuat dengan sebenarnya untuk dipergunakan sebagaimana mestinya.</td>
		    </tr>
		</table>

		<table border="1" style="font-size: 12px; text-align: justify; margin-top: 10px;">
			<tr>
		        <td style="text-align:center;">No</td>
		        <td style="text-align:center;">Jenis Dokumen</td>
		        <td style="text-align:center;">Keterangan</td>
		    </tr>
		    <tr>
		        <td style="text-align:center;">1</td>
		        <td style="text-align:left;">Surat Penawaran Pemberian Pembiayaan </td>
		        <td style="text-align:center;">ASLI</td>
		    </tr>
		    <tr>
		        <td style="text-align:center;">2</td>
		        <td style="text-align:left;">Perkiraan Biaya</td>
		        <td style="text-align:center;">ASLI</td>
		    </tr>
		    <tr>
		        <td style="text-align:center;">3</td>
		        <td style="text-align:left;">Syarat Syarat Umum Pembiayaan Retail</td>
		        <td style="text-align:center;">ASLI</td>
		    </tr>
		    <tr>
		        <td style="text-align:center;">4</td>
		        <td style="text-align:left;">Berita Acara</td>
		        <td style="text-align:center;">ASLI</td>
		    </tr>
		    <tr>
		        <td style="text-align:center;">5</td>
		        <td style="text-align:left;">Lampiran 13</td>
		        <td style="text-align:center;">ASLI</td>
		    </tr>
		    <tr>
		        <td style="text-align:center;">6</td>
		        <td style="text-align:left;">Jadwal Angsuran</td>
		        <td style="text-align:center;">ASLI</td>
		    </tr>
		    <tr>
		        <td style="text-align:center;">7</td>
		        <td style="text-align:left;">Surat Permohonan Realisasi Pembiayaan</td>
		        <td style="text-align:center;">ASLI</td>
		    </tr>
		    <tr>
		        <td style="text-align:center;">8</td>
		        <td style="text-align:left;">KYC & AML</td>
		        <td style="text-align:center;">ASLI</td>
		    </tr>
		    <tr>
		        <td style="text-align:center;">9</td>
		        <td style="text-align:left;">Dokumen Lainnya</td>
		        <td style="text-align:center;">ASLI</td>
		    </tr>

		</table>

		<div class="sign" style="margin-top: 10px; font-size: 12px;">
			
			<div class="left" style="width: 30%;">
				<p class="title" style="font-size: 12px;">PIHAK PERTAMA</p>
				<p class="title" style="font-size: 12px;">YANG MENYERAHKAN</p>
				<br>
				<br>
				<p style="font-size: 12px;">Rini Novita</p>
				<p style="font-size: 12px; font-style: italic;">Officer Pembiayaan</p>
			</div>
			
			<div class="middle" style="width: 40%; text-align: center;">
				<p class="title" style="font-size: 12px;">PIDAK KEDUA</p>
				<p class="title" style="font-size: 12px;">YANG MENERIMA PENYERAHAN</p>
				<br>
				<br>
				<p style="font-size: 12px;">Irmawati</p>
				<p style="font-size: 12px; font-style: italic;">Branch Manager</p>
			</div>

			<div class="right" style="width: 30%;">
				<p class="title" style="font-size: 12px;"></p>
				<p class="title" style="font-size: 12px;"></p>
				<br>
				<br>
				<br>
				<br>
				<p style="font-size: 12px;">Dwita Dellani</p>
				<p style="font-size: 12px; font-style: italic;">CBS</p>
			</div>
		</div><!-- / SIGN -->

	</body>
</html>