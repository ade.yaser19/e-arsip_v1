<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<div class="box">
    <div class="box-header">
    </div>
    <div class="box-body">
            <div class="form-group">
                <label>Surat Permohonan</label>
                <input type="text" name="surat_Permohonan" id='surat_Permohonan' class="form-control" placeholder="Surat Permohonan">
            </div>
            <div class="form-group">
                <label>Nama Nasabah</label>
                <input type="text" name="nama_nasabah" id ='nama_nasabah' class="form-control" placeholder="Nama Nasabah">
            </div>
            <div class="form-group">
                <label>No KTP</label>
                <input type="text" name="no_ktp" id='no_ktp' class="form-control" placeholder="No KTP">
            </div>
            <div class="form-group">
                <label>No Loan</label>
                <input type="text" name="no_loan"  id='no_loan' class="form-control" placeholder="No Loan">
            </div>
            <div class="form-group">
                <label>Tanggal Pencairan</label>
                <input type="date" name="tanggal_pencairan"  id ='tanggal_pencairan' class="form-control" placeholder="Tanggal Pencairan">
                <input type="hidden" name="status" id ='status' class="form-control" value="P">
            </div>
            <div class="form-group">
                <label>Alasan</label>
                <textarea name="" id="alasan" class="form-control"></textarea>
            </div>
            <!-- <a href="<?= base_url('Dpinjaman') ?>" class="btn btn-sm btn-warning" style="float: right;  margin-left: 5px;">Kembali</a> -->
            <button type="button" class="btn btn-sm btn-primary" id="simpan" style="float: right;">Simpan</button>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script type="text/javascript">
    $(document).ready(function (e) {
       $('#simpan').on('click', function () {
           var form_data = new FormData();
           var surat_Permohonan = document.getElementById("surat_Permohonan").value;
           var nama_nasabah = document.getElementById("nama_nasabah").value;
           var no_ktp = document.getElementById("no_ktp").value;
           var no_loan = document.getElementById("no_loan").value;
           var tanggal_pencairan = document.getElementById("tanggal_pencairan").value;
           var status = document.getElementById("status").value;
           var alasan = document.getElementById("alasan").value;

           form_data.append('surat_Permohonan', surat_Permohonan);
           form_data.append('nama_nasabah', nama_nasabah);
           form_data.append('no_ktp', no_ktp);
           form_data.append('no_loan', no_loan);
           form_data.append('tanggal_pencairan', tanggal_pencairan);
           form_data.append('status', status);
           form_data.append('alasan', alasan);

           $.ajax({
               url: '<?php echo base_url();?>/dpinjaman/do_add',
               data: form_data,
               dataType: 'text', // what to expect back from the server
               cache: false,
               contentType: false,
               processData: false,
               data: form_data,
               type: 'post',
               success: function (response) {
                var json = JSON.parse(response);
                 if(json.status =='E'){
                    swal_fn('error','Gagal','simpan data Gagal!'); 
                    setTimeout(function () { 
                       window.location = "<?= base_url('Dpinjaman') ?>";
                    },1000);
                 }else{
                    swal_fn('success','Sukses','simpan data berhasil!'); 
                    setTimeout(function () { 
                       window.location = "<?= base_url('Dpinjaman') ?>";
                    },1000);
                 }
               },
               error: function (response) {
                swal_fn('error','Gagal','simpan data Gagal!'); 
                    setTimeout(function () { 
                       window.location = "<?= base_url('Dpinjaman') ?>";
                    },1000);
               }
           });
       });
   });

    const swal_fn=(icon,title,text)=>{ Swal.fire({ icon: icon, title: title, text: text }); }
</script>