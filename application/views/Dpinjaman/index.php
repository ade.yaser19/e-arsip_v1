<div class="row">
    <div class="conttainer">
        <div class="col-lg-3 col-xs-6">
        </div>
    </div>
</div>
<div class="box">
    <div class="box-body">
        <form method="POST">
            <div class="box-body col-xs-12">
                  <div class="form-group row">
                    <label for="nama" class="col-sm-1 col-form-label">Nama</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="nama" name ='nama' placeholder="Nama">
                    </div>
                     <div class="col-sm-2">
                      <a href="<?= base_url('Dpinjaman/add'); ?>" class="btn btn-sm btn-flat btn-primary"><i class="glyphicon glyphicon-plus"></i> Create New</a> 
                    </div>
                </div>    

                <div class="form-group row">
                    <label for="tanggal" class="col-sm-1 col-form-label">Tanggal</label>
                    <div class="col-sm-8">
                      <input type="date" class="form-control" id="tanggal" name="tanggal">
                    </div>
                </div>     

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10 ">
                      <input type="submit" name="submit" value=" Cari" class="btn btn-sm btn-flat btn-primary">

                      <a href="<?= base_url('Dpinjaman'); ?>" class="btn btn-sm btn-flat btn-warning">Reset</a> 
                    </div>
                </div>      
            </div>
        </form>
    </div>

<div class="box">
 <!--    <div class="box-header">
        <h3 class="box-title">Data Pengguna</h3>
    </div> -->
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style="text-align: center; width:5px;">No</th>
                    <th>Tanggal</th>
                    <th>Nama Nasabah</th>
                    <th>keterangan</th>
                    <th>Status</th>
                    <th style="text-align: center;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $x = 1;
                foreach($dokumen as $v){?>
                <tr>
                    <td style="text-align: center;"><?= $x++; ?></td>
                    <td><?=$v['created_date']?></td>
                    <td><?=$v['nama_nasabah']?></td>
                    <td><?=$v['surat_Permohonan']?></td>
                    <td><?=$v['status']?></td>
                    <td style="text-align: center;">
                        <a href="<?= base_url('Dpinjaman/pdf_pinjaman/') . $v['id']; ?>" class="btn btn-flat btn-xs btn-danger">Print</a>
                    </td>
                </tr>
             <?php }?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->