<?= validation_errors(
	'<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
	'</div>'
); ?>


<div class="box">
	<div class="box-header">
		<h3>Tambah IDE E-Arsip</h3>
	</div>
	<div class="box-body">
		<form action="<?= base_url('arsip/add'); ?>" class="form" method="POST">
			
			<div class="row" style="margin-top:-10px;">	
				<div class="col-sm-6">
					<h5>Data Nasabah :</h5>
				</div>
			</div>

			<br/>
			
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label>Nama</label>
						<input type="text" class="form-control" name="nama" value="" required>
					</div>

					<div class="form-group">
						<label>No KTP</label>
						<input type="number" class="form-control" name="no_ktp" value="" required>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
						<label>No. HP</label>
						<input type="number" class="form-control" name="no_hp" value="" required>
					</div>

					<div class="form-group">
						<label>Email</label>
						<input type="email" class="form-control" name="email" value="" required>
					</div>
				</div>
			</div>

			<br/>

			<div class="row" style="margin-top:-10px;">	
				<div class="col-sm-6">
					<h5>Data Pembiayaan :</h5>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label>Loan</label>
						<input type="text" class="form-control" name="loan" value="" required>
					</div>

					<div class="form-group">
						<label>Produk</label>
						 <select name="produk" class="form-control" required>
		                  <option value="">- Pilih -</option>
			                  <option value="PRB">Pembelian Rumah Baru</option>
			                  <option value="PRS">Pembelian Rumah Bekas</option>
			                  <option value="TO">Take Over</option>
			                  <option value="FLPP">FLPP</option>
		                  </option>
		                </select>
					</div>

					<div class="form-group">
						<label>Plafond</label>
						<input type="text" class="form-control" name="plafound" value="" required>
					</div>

					<div class="form-group">
						<label>Tanggal Cair</label>
						<input type="date" class="form-control" name="tanggal_cair" value="" required>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
						<label>Developer</label>
						<select name="Developer" class="form-control">
		                  <option value="">- Pilih -</option>
			                  <option value="MAS">PT Mekar Agung Sejahtera</option>
			                  <option value="FPD">PT Fajar Putera Dinasti</option>
			                  <option value="KSP">PT Kirana Surya Perkasa</option>
			                  <option value="CGS">PT Cipta Graha Sejahtera</option>
			                  <option value="CGA">PT Cipta Graha Adijaya</option>
			                  <option value="ML">PT Metropolitan Land</option>
		                  </option>
		                </select>
					</div>

					<div class="form-group">
						<label>Nama Proyek</label>
						<input type="text" class="form-control" name="nama_proyek" value="">
					</div>

					<div class="form-group">
		                <label>Cabang</label>
		                <select name="cabang" class="form-control" required>
		                  <option value="">- Pilih -</option>
			                  <option value="SMB">10071 - Bekasi Summarecon</option>
			                  <option value="AY">10072 - Bekasi Ahmad Yani</option>
			                  <option value="TIMUR">10073 - Bekasi Timur</option>
			                  <option value="TAMBUN">10074 - Bekasi Tambun</option>
			                  <option value="CIKARANG">10075 - Bekasi Cikarang</option>
		                  </option>
		                </select>
		            </div>

				</div>
			</div>
			
			<a href="<?= base_url('arsip') ?>" class="btn btn-sm btn-warning" style ="float: right; margin-left: 5px;">Kembali</a>
			<button type="submit" class="btn btn-sm btn-primary" style ="float: right;">Simpan</button>
		</form>	
	</div>
</div>