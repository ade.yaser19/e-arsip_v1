<div class="row">
    <div class="conttainer">
        <div class="col-lg-3 col-xs-6">
        </div>
    </div>
</div>
<div class="box">
    <div class="box-body">
        <form method="POST">
        <div class="box-body col-xs-12">
              <div class="form-group row">
                <label for="nama" class="col-sm-1 col-form-label">Nama</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="nama" name ='nama' placeholder="Nama">
                </div>
            </div>    

            <div class="form-group row">
                <label for="tanggal" class="col-sm-1 col-form-label">Tanggal</label>
                <div class="col-sm-8">
                  <input type="date" class="form-control" id="tanggal" name="tanggal">
                </div>
            </div>     

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10 ">
                    <input type="submit" name="Cari" class="btn btn-sm btn-flat btn-primary" value="Cari" />

                  <a href="<?= base_url('upload'); ?>" class="btn btn-sm btn-flat btn-warning"><i class="glyphicon glyphicon-erase"></i> Reset </a> 
                </div>
            </div>      
        </div>
    </form>
    </div>

 <!-- /.box-header -->
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style="text-align: center; width:5px;">No</th>
                    <th>No E-Arsip</th>
                    <th>Tanggal</th>
                    <th>Nama Nasabah</th>
                    <th>Loan</th>
                    <th>Plafond</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $x = 1;
                foreach ($arsip as $v) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $x++; ?></td>
                        <td><a href="<?= base_url('upload/add').'/'.$v['id']; ?>" class="btn btn-sm btn-flat btn-primary"><?= $v['id']; ?> </a> </td>
                        <td><?= $v['created_date']; ?></td>
                        <td><?= $v['nama']; ?></td>
                        <td><?= $v['loan']; ?></td>
                        <td><?= $v['plafound']; ?></td>
                        <td><?= $v['status']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->