<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<div class="box">
	<div class="box-header">
		<h3>Upload E-Arsip</h3>
	</div>
	<div class="box-body">
			<div class="row" style="margin-top:-10px;">	
				<div class="col-sm-6">
					<h5>Data Nasabah :</h5>
				</div>
			</div>

			<br/>

			<div class="row">
				<div class="col-sm-12">
			    <table class="table table-sm"> 
					<div class="form-group">
						<tr>
							<td>Nama</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="nama" value="<?=$arsip[0]['nama']?>" readonly></td>
							<td>No HP</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="hp" value="<?=$arsip[0]['no_hp']?>" readonly></td>
						</tr>
						<tr>
							<td>No KTP</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="ktp" value="<?=$arsip[0]['no_ktp']?>" readonly></td>
							<td>Email</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="email" value="<?=$arsip[0]['email']?>" readonly></td>
						</tr>
					</div>
			 	</table>
				</div>
			</div>

			<br/>

			<div class="row" style="margin-top:-10px;">	
				<div class="col-sm-6">
					<h5>Data Pembiayaan :</h5>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-3">
					<table class="table table-sm">
					<tr>
						<td>Loan</td>
						<td>:</td>
						<td><?=$arsip[0]['loan']?></td>
					</tr>
					<tr>
						<td>Produk</td>
						<td>:</td>
						<td><?=$arsip[0]['produk']?></td>
					</tr>
					<tr>
						<td>Plafound</td>
						<td>:</td>
						<td><?=$arsip[0]['plafound']?></td>
					</tr>
					<tr>
						<td>Tanggal cair</td>
						<td>:</td>
						<td><?=$arsip[0]['tanggal_cair']?></td>
					</tr>
					<tr>
						<td>Developer</td>
						<td>:</td>
						<td><?=$arsip[0]['Developer']?></td>
					</tr>
					<tr>
						<td>Nama Projeck</td>
						<td>:</td>
						<td><?=$arsip[0]['nama_proyek']?></td>
					</tr>
					<tr>
						<td>Cabang</td>
						<td>:</td>
						<td><?=$arsip[0]['cabang']?></td>
					</tr>
					</table>
				</div>
			</div>

			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#UploadModal">
			Tambah Dokumen</button>
			<hr style="height:2px; width:100%; border-width:0; color:green; background-color:green">
			
			<div class="row">
				<div class="col-sm-12">
			    <table class="table table-bordered" id="myTable" border = 1>
				  <thead>
				    	<tr>
				     	 	<th scope="col">No</th>
				      		<th scope="col">Nama Dokumen</th>
				     	 	<th scope="col">Action</th>
				    	</tr>
				  	</thead><tbody></tbody>
				</table>
				</div>
			</div>
		</form>	
	</div>
</div>

<div class="modal fade" id="UploadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload E-Arsip</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-sm">
		 <tr>
			<td>Nama Dokumen</td>
			<td>:</td>
			<td><input type="text" class="form-control" name="dokumen" id="dokumen"></td>
			<td><input type="hidden" class="form-control" name="id" id="id"></td>
		</tr>
		<tr>
			<td>Upload Dokumen</td>
			<td>:</td>
			<td><input type="file" class="form-control" name="file" id ="file" value=""></td>
		</tr>
		</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary"  id="upload">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ViewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id='label_dokumen'></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id='showDokumen' style="height:800px;">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script type="text/javascript">
$( document ).ready(function() {
  //  console.log( "ready!" );
	loaddata();
});

  function showModalupload(id,namadokumen){
  	$('#UploadModal').modal('show');
    document.getElementById("dokumen").value =namadokumen;
	document.getElementById('id').value = id;
  }

  function loaddata(){
  	var id_arsip = '<?php echo $id_arsip?>';
  	console.log(id_arsip);
  	$.ajax({
  		 url:'<?php echo base_url();?>/upload/show',
  		 type :"POST",
  		 data :{arsip_id :'<?php echo $id_arsip?>'},
  		 success: function(response){
  		 	var no = 1;
			var todos = JSON.parse(response);
			todos.forEach(function(value, index){
				$("#myTable").append(`
				<tr>
					<td>${no}</td>
					<td>${value.nama_dokumen}</td>
					<td><button type="submit" class="btn btn-sm btn-primary" id="upload" onclick="showModalupload('${value.id}','${value.nama_dokumen}')">Upload</button>
    				  <button type="submit" class="btn btn-sm btn-warning" id="view" onclick="dokumen_view('${value.nama_dokumen}','${value.link_dokumen}')">View</button></td>
				</tr>`);
				no++;
			})
		}
  	});
  }

</script>

<script type="text/javascript">
   $(document).ready(function (e) {
       $('#upload').on('click', function () {
           var file_data = $('#file').prop('files')[0];
           var form_data = new FormData();
           var id_arsip ='<?php echo $id_arsip?>';
           var dokumen = document.getElementById("dokumen").value;
           var id = document.getElementById("id").value;
           form_data.append('file', file_data);
           form_data.append('id_arsip', id_arsip);
           form_data.append('dokumen', dokumen);
           form_data.append('id', id);
           $.ajax({
               url: '<?php echo base_url();?>/upload/upload_file', // point to server-side controller method
               dataType: 'text', // what to expect back from the server
               cache: false,
               contentType: false,
               processData: false,
               data: form_data,
               type: 'post',
               success: function (response) {
               	var json = JSON.parse(response);
               	 if(json.status =='E'){
                    swal_fn('error','Gagal','simpan data Gagal!'); 
                    $('#UploadModal').modal('hide');
                    setTimeout(function () { 
      					location.reload();
    				},1000);
               	 }else{
                  	swal_fn('success','Sukses','simpan data berhasil!'); 
                  	$('#UploadModal').modal('hide');
                  	setTimeout(function () { 
      					location.reload();
    				},1000);
               	 }
               },
               error: function (response) {
               	swal_fn('error','Gagal','simpan data Gagal!'); 
               	$('#UploadModal').modal('hide');
               	setTimeout(function () { 
      					location.reload();
    				},1000);
               }
           });
       });
   });

   	const swal_fn=(icon,title,text)=>{ Swal.fire({ icon: icon, title: title, text: text }); }

   function dokumen_view(dokumen,link){
   		var link_new = link.replace(/[\/\\#, +$~%'":*?<>{}]/g, '_');
   		var real_link ='<?php echo base_url();?>'+'uploads/'+link_new;
   		$('#label_dokumen').html(dokumen);
		$('#showDokumen').html(`<iframe src="${real_link}" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height:100%;width:100%;position:absolute;top:0px;left:0px;right:0px;bottom:0px" height="100%" width="100%"></iframe>`);
		$('#ViewModal').modal('show');
	}

 </script>