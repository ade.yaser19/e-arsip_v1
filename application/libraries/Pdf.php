<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// include_once APPPATH.'/third_party/MPDF/src/Mpdf.php';
include_once APPPATH.'/third_party/MPDF/vendor/autoload.php';

class pdf {

   public $param;
   public $pdf;

   public function __construct()
   {
        // $this->param =$param;
        // $this->pdf = new mPDF($this->param);
        $this->pdf = new \Mpdf\Mpdf();
   }
}