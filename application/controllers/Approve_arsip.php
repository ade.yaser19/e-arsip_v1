<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Approve_arsip extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Approvearsip_model', 'approve_arsip');
    }

    public function index()
    {
    	$user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $data = [
            'head'          => 'Approval E-Arsip',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $data['approve_arsip'] = $this->approve_arsip->get();
        foreach ($data['approve_arsip'] as $r => $value) {

            if ($value['cabang'] == 'SMB') {
            
                $data['approve_arsip'][$r]['cabang'] = '10071 - Bekasi Summarecon';
            
            } elseif ($value['cabang'] == 'AY') {

                $data['approve_arsip'][$r]['cabang'] = '10072 - Ahmad Yani';
            
            } elseif ($value['cabang'] == 'TIMUR') {

                $data['approve_arsip'][$r]['cabang'] = '10073 - Bekasi Timur';
            
            } elseif ($value['cabang'] == 'TAMBUN') {

                $data['approve_arsip'][$r]['cabang'] = '10074 - Bekasi Tambun';
            
            } else {
            
                $data['approve_arsip'][$r]['cabang'] = '10074 - Bekasi Cikarang';
            
            }


        }
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('approve_arsip/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {

        $post = $this->input->post();
            $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
            $name           = $user['nama'];
            $img            = $user['img'];
            $date_created   = $user['date_created'];
            $data = [
                'head'          => 'Approval E-Arsip',
                'name'          => $name,
                'img'           => $img,
                'date_created'  => $date_created
            ];

            $get = $this->approve_arsip->detail();

            if ($get['produk'] == 'PRB') {
                $get['produk'] = 'Pembelian Rumah Baru';
            } else if ($get['produk'] == 'PRS') {
                $get['produk'] = 'Pembelian Rumah Bekas';
            } else if ($get['produk'] == 'TO') {
                $get['produk'] = 'Take Over';
            } else {
                $get['produk'] = 'FLPP';
            }

            if ($get['Developer'] == 'MAS') {
                $get['Developer'] = 'PT Mekar Agung Sejahtera';
            } else if ($get['Developer'] == 'FPD') {
                $get['Developer'] = 'PT Fajar Putera Dinasti';
            } else if ($get['Developer'] == 'KSP') {
                $get['Developer'] = 'PT Kirana Surya Perkasa';
            } else if ($get['Developer'] == 'CGS') {
                $get['Developer'] = 'PT Cipta Graha Sejahtera';
            } else if ($get['Developer'] == 'CGA') {
                $get['Developer'] = 'PT Cipta Graha Adijaya';
            } else {
                $get['Developer'] = 'PT Metropolitan Land';
            }

            if ($get['cabang'] == 'SMB') {
                $get['cabang'] = '10071 - Bekasi Summarecon';
            } else if ($get['cabang'] == 'AY') {
                $get['cabang'] = '10072 - Bekasi Ahmad Yani';
            } else if ($get['cabang'] == 'TIMUR') {
                $get['cabang'] = '10073 - Bekasi Timur';
            } else if ($get['cabang'] == 'TAMBUN') {
                $get['cabang'] = '10074 - Bekasi Tambun';
            } else {
                $get['cabang'] = '10075 - Bekasi Cikarang';
            }

            $this->load->view('templates/head', $data);
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('approve_arsip/edit', $get);
            $this->load->view('templates/footer');
    }

    public function detail($id = null)
    {

        $post = $this->input->post();
            $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
            $name           = $user['nama'];
            $img            = $user['img'];
            $date_created   = $user['date_created'];
            $data = [
                'head'          => 'Detail Approval E-Arsip',
                'name'          => $name,
                'img'           => $img,
                'date_created'  => $date_created
            ];

            $get = $this->approve_arsip->detail();

            if ($get['produk'] == 'PRB') {
                $get['produk'] = 'Pembelian Rumah Baru';
            } else if ($get['produk'] == 'PRS') {
                $get['produk'] = 'Pembelian Rumah Bekas';
            } else if ($get['produk'] == 'TO') {
                $get['produk'] = 'Take Over';
            } else {
                $get['produk'] = 'FLPP';
            }

            if ($get['Developer'] == 'MAS') {
                $get['Developer'] = 'PT Mekar Agung Sejahtera';
            } else if ($get['Developer'] == 'FPD') {
                $get['Developer'] = 'PT Fajar Putera Dinasti';
            } else if ($get['Developer'] == 'KSP') {
                $get['Developer'] = 'PT Kirana Surya Perkasa';
            } else if ($get['Developer'] == 'CGS') {
                $get['Developer'] = 'PT Cipta Graha Sejahtera';
            } else if ($get['Developer'] == 'CGA') {
                $get['Developer'] = 'PT Cipta Graha Adijaya';
            } else {
                $get['Developer'] = 'PT Metropolitan Land';
            }

            if ($get['cabang'] == 'SMB') {
                $get['cabang'] = '10071 - Bekasi Summarecon';
            } else if ($get['cabang'] == 'AY') {
                $get['cabang'] = '10072 - Bekasi Ahmad Yani';
            } else if ($get['cabang'] == 'TIMUR') {
                $get['cabang'] = '10073 - Bekasi Timur';
            } else if ($get['cabang'] == 'TAMBUN') {
                $get['cabang'] = '10074 - Bekasi Tambun';
            } else {
                $get['cabang'] = '10075 - Bekasi Cikarang';
            }

            if ($get['status'] == 'P') {
                $get['status'] = 'Pending Approval';
            } else if ($get['status'] == 'R') {
                $get['status'] = 'Reject';
            } else {
                $get['status'] = 'Approved';
            }

            $this->load->view('templates/head', $data);
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('approve_arsip/detail', $get);
            $this->load->view('templates/footer');
    }

    public function reject()
    {
        $id = $this->uri->segment(3);
        $data = [
            'id'    => $id,
            'status' => 'R'
        ];
        $this->approve_arsip->update($data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil direject
            </div>');
        redirect('approve_arsip');
    }

    public function approve()
    {
        $id = $this->uri->segment(3);
        $data = [
            'id'    => $id,
            'status' => 'A'
        ];
        $this->approve_arsip->update($data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil diapprove
            </div>');
        redirect('approve_arsip');
    }

}

/* End of file Approve_arsip.php */
/* Location: ./application/controllers/Approve_arsip.php */