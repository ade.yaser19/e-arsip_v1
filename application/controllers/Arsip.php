<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Arsip extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Arsip_model', 'arsip');
    }

    public function index()
    {
    	$user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $data = [
            'head'          => 'IDE E-Arsip',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $data['arsip'] = $this->arsip->get();
        foreach ($data['arsip'] as $r => $value) {
            if ($value['status'] == 'P') {
                $data['arsip'][$r]['status'] = 'Pending Approval';
            } else if ($value['status'] == 'R') {
                $data['arsip'][$r]['status'] = 'Reject';
            } else {
                $data['arsip'][$r]['status'] = 'Approved';
            }
        }

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('arsip/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {   
        $post = $this->input->post();

        if ($post) {

            $data = [
                'id'    => $this->arsip->CreateCode(),
                'nama' => $post['nama'],
                'no_ktp' => $post['no_ktp'],
                'no_hp' => $post['no_hp'],
                'email' => $post['email'],
                'loan' => $post['loan'],
                'produk' => $post['produk'],
                'plafound' => $post['plafound'],
                'tanggal_cair' => $post['tanggal_cair'],
                'Developer' => $post['Developer'],
                'nama_proyek' => $post['nama_proyek'],
                'cabang' => $post['cabang'],
                'status' => 'P',
                'created_date' => date('Y-m-d'),
            ];
            $this->arsip->save($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            Hak akses Menu berhasil ditambah
            </div>');
            redirect('arsip');
        
        } else {

            $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
            $name           = $user['nama'];
            $img            = $user['img'];
            $date_created   = $user['date_created'];
            $data = [
                'head'          => 'IDE E-Arsip',
                'name'          => $name,
                'img'           => $img,
                'date_created'  => $date_created
            ];

            $this->load->view('templates/head', $data);
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('arsip/add', $data);
            $this->load->view('templates/footer');
        }
    }
}

/* End of file Arsip.php */
/* Location: ./application/controllers/Arsip.php */