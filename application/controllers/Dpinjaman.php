<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dpinjaman extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        is_logged_in();
       $this->load->model('Mpinjaman', 'mpinjaman');
    }

    public function index()
    {
        $nama           ='';
        $tanggal        ='';
        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $data = [
            'head'          => 'Peminjaman Dokumen',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];
        if(isset($_POST['nama']) && $_POST['nama'] !=''){
            $nama = $_POST['nama'];
        }

        if(isset($_POST['tanggal']) && $_POST['tanggal'] !=''){
            $tanggal = $_POST['tanggal'];
        }

        $data['dokumen'] = $this->mpinjaman->search_data($nama,$tanggal);

        foreach ($data['dokumen'] as $r => $value) {

            if ($value['status'] == 'P') {
                $data['dokumen'][$r]['status'] = 'Pending Approval';
            } else if ($value['status'] == 'R') {
                $data['dokumen'][$r]['status'] = 'Reject';
            } else {
                $data['dokumen'][$r]['status'] = 'Approved';
            }
        }
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('Dpinjaman/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {

        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $data = [
            'head'          => 'Peminjaman Dokumen',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];
        

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('Dpinjaman/add', $data);
        $this->load->view('templates/footer');
    }

     public function do_add()
    {
       $param  = ($_POST);
       $simpan = $this->mpinjaman->save($param);
       if($simpan){
          echo json_encode(array('status'=>'S','msg'=>'Data Sukses Disimpan'));
       }else{
          echo json_encode(array('status'=>'E','msg'=>'Data Gagal Disimpan'));
       }
    }

    public function pdf_pinjaman($id)
    {
        $data_pdf              = $this->mpinjaman->getpeminjamanById($id);
       
        if ($data_pdf->cabang == 'SMB') {
            
            $data_pdf->cabang = '10071 - Bekasi Summarecon';
        
        } elseif ($data_pdf->cabang == 'AY') {

            $data_pdf->cabang = '10072 - Ahmad Yani';
        
        } elseif ($data_pdf->cabang == 'TIMUR') {

            $data_pdf->cabang = '10073 - Bekasi Timur';
        
        } elseif ($data_pdf->cabang == 'TAMBUN') {

            $data_pdf->cabang = '10074 - Bekasi Tambun';
        
        } else {
        
            $data_pdf->cabang = '10074 - Bekasi Cikarang';
        
        }

         if ($data_pdf) {
            $html = $this->load->view('Dpinjaman/laporan_pdf', [
                'data'  => $data_pdf
            ], true);
            $this->load->library('Pdf');
            $this->pdf->pdf->AddPage('P');
            $this->pdf->pdf->WriteHTML($html);
            $this->pdf->pdf->Output('BERITA ACARA SERAH TERIMA DOKUMEN'.'.pdf', 'I');
        } else {
            redirect('Dpinjaman');
        }
    }
}

/* End of file IdeArsip.php */
/* Location: ./application/controllers/IdeArsip.php */