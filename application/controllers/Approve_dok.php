<?php 

defined('BASEPATH') or exit('No direct script access allowed');

class Approve_dok extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Approvedok_model', 'approve_dok');
    }

    public function index()
    {
        $nama           ='';
        $tanggal        ='';
    	$user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $data = [
            'head'          => 'Approval Dokumen',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        if(isset($_POST['nama']) && $_POST['nama'] !=''){
            $nama = $_POST['nama'];
        }

        if(isset($_POST['tanggal']) && $_POST['tanggal'] !=''){
            $tanggal = $_POST['tanggal'];
        }

        $data['approve_dok'] = $this->approve_dok->search_data($nama, $tanggal);

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('approve_dok/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {

        $post = $this->input->post();
            $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
            $name           = $user['nama'];
            $img            = $user['img'];
            $date_created   = $user['date_created'];
            $data = [
                'head'          => 'Approval Dokumen',
                'name'          => $name,
                'img'           => $img,
                'date_created'  => $date_created
            ];

            $get = $this->approve_dok->detail();

            $this->load->view('templates/head', $data);
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('approve_dok/edit', $get);
            $this->load->view('templates/footer');
    }

    public function detail($id = null)
    {

        $post = $this->input->post();
            $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
            $name           = $user['nama'];
            $img            = $user['img'];
            $date_created   = $user['date_created'];
            $data = [
                'head'          => 'Detail Approval Dokumen',
                'name'          => $name,
                'img'           => $img,
                'date_created'  => $date_created
            ];

            $get = $this->approve_dok->detail();

            if ($get['status'] == 'P') {
                $get['status'] = 'Pending Approval';
            } else if ($get['status'] == 'R') {
                $get['status'] = 'Reject';
            } else {
                $get['status'] = 'Approved';
            }
            
            $this->load->view('templates/head', $data);
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('approve_dok/detail', $get);
            $this->load->view('templates/footer');
    }

    public function reject()
    {
        $id = $this->uri->segment(3);
        $data = [
            'id'    => $id,
            'status' => 'R'
        ];
        $this->approve_dok->update($data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil direject
            </div>');
        redirect('approve_dok');
    }

    public function approve()
    {
        $id = $this->uri->segment(3);
        $data = [
            'id'    => $id,
            'status' => 'A'
        ];
        $this->approve_dok->update($data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil diapprove
            </div>');
        redirect('approve_dok');
    }

}

/* End of file Approve_dok.php */
/* Location: ./application/controllers/Approve_dok.php */