<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_earsip extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        is_logged_in();
       $this->load->model('Arsip_model', 'arsip');
    }

    public function index()
    {

        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $nama           ='';
        $tanggal        ='';
        $date_created   = $user['date_created'];
        $data = [
            'head'          => 'Master E-Arsip',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        if(isset($_POST['nama']) && $_POST['nama'] !=''){
            $nama = $_POST['nama'];
        }

        if(isset($_POST['tanggal']) && $_POST['tanggal'] !=''){
            $tanggal = $_POST['tanggal'];
        }

        $data['arsip'] = $this->arsip->search_data($nama,$tanggal);
        foreach ($data['arsip'] as $r => $value) {

            if ($value['status'] == 'P') {
                $data['arsip'][$r]['status'] = 'Pending Approval';
            } else {
                $data['arsip'][$r]['status'] = 'Approved';
            }

            if ($value['produk'] == 'PRB') {
                $data['arsip'][$r]['produk'] = 'Pembelian Rumah Baru';
            } else if ($value['produk'] == 'PRS') {
                $data['arsip'][$r]['produk'] = 'Pembelian Rumah Bekas';
            } else if ($value['produk'] == 'TO') {
                $data['arsip'][$r]['produk'] = 'Take Over';
            } else {
                $data['arsip'][$r]['produk'] = 'FLPP';
            }

            if ($value['cabang'] == 'SMB') {
                $data['arsip'][$r]['cabang'] = '10071 - Bekasi Summarecon';
            } else if ($value['cabang'] == 'AY') {
                $data['arsip'][$r]['cabang'] = '10072 - Bekasi Ahmad Yani';
            } else if ($value['cabang'] == 'TIMUR') {
                $data['arsip'][$r]['cabang'] = '10073 - Bekasi Timur';
            } else if ($value['cabang'] == 'TAMBUN') {
                $data['arsip'][$r]['cabang'] = '10074 - Bekasi Tambun';
            } else {
                $data['arsip'][$r]['cabang'] = '10075 - Bekasi Cikarang';
            }
        }

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('master/grid', $data);
        $this->load->view('templates/footer');
    }

    public function view_detail()
    {

        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $data = [
            'head'          => '',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $data['id_arsip'] = $this->uri->segment(3);

        $data['arsip'] = $this->arsip->get_to_add($this->uri->segment(3));
        foreach ($data['arsip'] as $r => $value) {
            if ($value['status'] == 'P') {
                $data['arsip'][$r]['status'] = 'Pending Approval';
            } else {
                $data['arsip'][$r]['status'] = 'Approve Approval';
            }

            if ($value['produk'] == 'PRB') {
                $data['arsip'][$r]['produk'] = 'Pembelian Rumah Baru';
            } else if ($value['produk'] == 'PRS') {
                $data['arsip'][$r]['produk'] = 'Pembelian Rumah Bekas';
            } else if ($value['produk'] == 'TO') {
                $data['arsip'][$r]['produk'] = 'Take Over';
            } else {
                $data['arsip'][$r]['produk'] = 'FLPP';
            }

            if ($value['Developer'] == 'MAS') {
                $data['arsip'][$r]['Developer'] = 'PT Mekar Agung Sejahtera';
            } else if ($value['Developer'] == 'FPD') {
                $data['arsip'][$r]['Developer'] = 'PT Fajar Putera Dinasti';
            } else if ($value['Developer'] == 'KSP') {
                $data['arsip'][$r]['Developer'] = 'PT Kirana Surya Perkasa';
            } else if ($value['Developer'] == 'CGS') {
                $data['arsip'][$r]['Developer'] = 'PT Cipta Graha Sejahtera';
            } else if ($value['Developer'] == 'CGA') {
                $data['arsip'][$r]['Developer'] = 'PT Cipta Graha Adijaya';
            } else {
                $data['arsip'][$r]['Developer'] = 'PT Metropolitan Land';
            }

            if ($value['cabang'] == 'SMB') {
                $data['arsip'][$r]['cabang'] = '10071 - Bekasi Summarecon';
            } else if ($value['cabang'] == 'AY') {
                $data['arsip'][$r]['cabang'] = '10072 - Bekasi Ahmad Yani';
            } else if ($value['cabang'] == 'TIMUR') {
                $data['arsip'][$r]['cabang'] = '10073 - Bekasi Timur';
            } else if ($value['cabang'] == 'TAMBUN') {
                $data['arsip'][$r]['cabang'] = '10074 - Bekasi Tambun';
            } else {
                $data['arsip'][$r]['cabang'] = '10075 - Bekasi Cikarang';
            }
        }

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('master/view', $data);
        $this->load->view('templates/footer');
    }

}
