<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Arsip_model extends CI_Model {

	public function get()
    {
        $this->db->select('id, tanggal_cair, nama, loan, plafound, status,no_ktp,no_hp,developer,nama_proyek,cabang,email,produk, created_date');
        $this->db->from('th_id_arsip');
        $this->db->order_by('created_date','DESC');
        return $this->db->get()->result_array();
    }

    public function CreateCode() {
        $this->db->select('RIGHT(id,6) as kode', FALSE);
        $this->db->order_by('id','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('th_id_arsip');
            if($query->num_rows() <> 0){      
                 $data = $query->row();
                 $kode = intval($data->kode) + 1; 
            }
            else{      
                 $kode = 1;  
            }
        $batas = str_pad($kode, 6, "0", STR_PAD_LEFT);    
        $kodetampil = "AP".$batas;
        return $kodetampil;  
    }

    public function save($data)
    {
        $this->db->insert('th_id_arsip', $data);
    }

    public function get_detail($id_arsip){
        $this->db->select('id, id_arsip, nama_dokumen,link_dokumen');
        $this->db->from('ttd_id_arsip');
        $this->db->where('id_arsip',$id_arsip);
        return $this->db->get()->result_array();
    }

    public function upload($data,$id_arsip,$id){

        if($id !=''){
            return  $this->db->where('id', $id)
                    ->where('id_arsip',$id_arsip)
                    ->update('ttd_id_arsip',$data);
        }else{
            return  $this->db->insert('ttd_id_arsip', $data);
        }
    }

    public function get_to_add($id){
        $this->db->select('id, tanggal_cair, nama, loan, plafound, status,no_ktp,no_hp,Developer,nama_proyek,cabang,email,produk');
        $this->db->from('th_id_arsip');
        $this->db->where('id',$id);
        return $this->db->get()->result_array();
    }

    public function search_data($nama='',$tanggal=''){
        $this->db->select('id, tanggal_cair, nama, loan, plafound, status,no_ktp,no_hp,developer,nama_proyek,cabang,email,produk, created_date');
        $this->db->from('th_id_arsip');
        if($nama !=''){
            $this->db->like('nama', $nama);
        }
        if($tanggal !=''){
            $this->db->where('tanggal_cair =', $tanggal);
        }
        $this->db->order_by('created_date','DESC');
        return $this->db->get()->result_array();
    }


}

/* End of file Arsip_model.php */
/* Location: ./application/models/Arsip_model.php */