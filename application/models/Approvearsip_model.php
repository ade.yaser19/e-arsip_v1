<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Approvearsip_model extends CI_Model {

	public function get()
    {
        $this->db->select('id, tanggal_cair, nama, loan, cabang, status, created_date');
        $this->db->from('th_id_arsip');
        $this->db->order_by('created_date','DESC');
        return $this->db->get()->result_array();
    }

    public function detail()
    {
        $id = $this->uri->segment(3);
        return $this->db->get_where('th_id_arsip', ['id' => $id])->row_array();
    }

    public function update($data)
    {
        $id = $this->uri->segment(3);
        $this->db->update('th_id_arsip', $data, ['id' => $id]);
    }

}

/* End of file Approvearsip_model.php */
/* Location: ./application/models/Approvearsip_model.php */