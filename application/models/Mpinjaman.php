<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpinjaman extends CI_Model {

	public function search_data($nama='',$tanggal='')
    {
        $this->db->select('id, surat_Permohonan, nama_nasabah, no_ktp, no_loan, tanggal_pencairan, alasan, created_date, status');
        $this->db->from('tr_pd');
        if($nama !=''){
            $this->db->like('nama_nasabah', $nama);
        }
        if($tanggal !=''){
            $this->db->where('tanggal_pencairan =', $tanggal);
        }
        $this->db->order_by('created_date','DESC');
        return $this->db->get()->result_array();
    }

    public function save($data){
        return  $this->db->insert('tr_pd', $data);
    }

    public function getpeminjamanById($id) {
        return $this->db->query("
            SELECT tr_pd.created_date, tr_pd.nama_nasabah, tr_pd.surat_Permohonan, th_id_arsip.cabang 
            FROM tr_pd
            LEFT JOIN th_id_arsip on th_id_arsip.loan = tr_pd.no_loan 
            WHERE tr_pd.id = '$id'
        ")->row_object();
    }
}

/* End of file Arsip_model.php */
/* Location: ./application/models/Arsip_model.php */