<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Approvedok_model extends CI_Model {

    public function search_data($nama='',$tanggal='')
    {
        $this->db->select('*');
        $this->db->from('tr_pd');
        if($nama !=''){
            $this->db->like('nama_nasabah', $nama);
        }
        if($tanggal !=''){
            $this->db->where('tanggal_pencairan =', $tanggal);
        }
        $this->db->order_by('created_date','DESC');

        return $this->db->get()->result_array();
    }
    
	// public function get()
    // {
    //     $this->db->select('id, tanggal_pencairan, nama_nasabah, alasan, status');
    //     $this->db->from('tr_pd');
    //     return $this->db->get()->result_array();
    // }

    public function detail()
    {
        $id = $this->uri->segment(3);
        return $this->db->get_where('tr_pd', ['id' => $id])->row_array();
    }

    public function update($data)
    {
        $id = $this->uri->segment(3);
        $this->db->update('tr_pd', $data, ['id' => $id]);
    }

}

/* End of file Approvedok_model.php */
/* Location: ./application/models/Approvedok_model.php */